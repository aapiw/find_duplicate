require "pry"
require 'digest/md5'
require 'progress_bar'

def check_duplicated path
	include ProgressBar::WithProgress
	_hash = {}
	Dir.chdir(path)
	puts "loading..."
	Dir.glob("**/*", File::FNM_DOTMATCH).each_with_progress do |filename|
	  next if File.directory?(filename)
	  key = Digest::MD5.hexdigest(IO.read(filename))
	  if _hash.has_key? key
	    _hash[key].push filename
	  else
	    _hash[key] = [filename]
	  end
	end

	values = _hash.values.max
	if values.length > 1
		puts IO.read(values.first)+" "+ values.size.to_s
	else
		"No Duplicate"
	end
end
path = "./DropsuiteTest"
# path = "/home/aapiw/Documents/Projects/test"
check_duplicated(path)